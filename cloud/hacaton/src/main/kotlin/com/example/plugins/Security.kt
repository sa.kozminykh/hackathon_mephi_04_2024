package com.example.plugins

import com.example.data.users.UserRepo
import io.ktor.server.application.*
import io.ktor.server.auth.*

fun Application.configureSecurity(
    userRepo: UserRepo
) {
    install(Authentication) {
        basic("auth-basic") {
//            realm = ""
            validate { credentials ->
                if (userRepo.checkUserPassword(credentials.name, credentials.password)) {
                    UserIdPrincipal(credentials.name)
                } else {
                    null
                }
            }
        }
    }
}
