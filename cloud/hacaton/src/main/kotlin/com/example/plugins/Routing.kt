package com.example.plugins

import com.example.data.users.UserRepo
import com.example.data.usersToContainers.UserToContainerRepo
import com.example.dtos.ContainerDTO
import com.example.dtos.UserDTO
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.routing.get

fun Application.configureRouting(
    userRepo: UserRepo,
    userToContainerRepo: UserToContainerRepo
) {
    routing {
        authenticate("auth-basic") {
            get("/user") {
                call.respond(call.principal<UserIdPrincipal>()?.name.toString())
            }
            get("/containers") {
                val containers = userToContainerRepo
                    .getContainers(call.principal<UserIdPrincipal>()?.name.toString())
                if (containers == null) {
                    call.respond(HttpStatusCode.NoContent, "No such user")
                } else {
                    call.respond(containers)
                }
            }
            post("/container") {
                val container = call.receive<ContainerDTO>()
                val res = userToContainerRepo.addContainer(
                    call.principal<UserIdPrincipal>()?.name.toString(),
                    container
                )
                if (res == null) {
                    call.respond(HttpStatusCode.NoContent, "No such user")
                } else {
                    call.respond(HttpStatusCode.OK)
                }
            }
        }
        get("/") {
            call.respondText("Hello World!")
        }
        post("/user") {
            val user = call.receive<UserDTO>()
            val newUser = userRepo.createUser(user.login, user.password)
            if (newUser != null) call.respond(HttpStatusCode.OK)
            else call.respond(HttpStatusCode.Conflict)
        }


    }
}
