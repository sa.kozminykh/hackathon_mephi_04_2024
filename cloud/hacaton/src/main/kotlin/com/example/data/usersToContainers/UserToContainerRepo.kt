package com.example.data.usersToContainers

import com.example.data.container.Container
import com.example.data.container.ContainerRepo
import com.example.data.container.Containers
import com.example.data.users.User
import com.example.data.users.UserRepo
import com.example.data.users.Users
import com.example.dtos.ContainerDTO
import com.example.dtos.asResponse
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import kotlin.coroutines.CoroutineContext

class UserToContainerRepo(
    private val userRepo: UserRepo,
    private val containerRepo: ContainerRepo,
    private val database: Database,
    private val context: CoroutineContext = Dispatchers.IO
) {
    suspend fun addContainer(userLogin: String, containerDTO: ContainerDTO) = run {

        newSuspendedTransaction(context, database) {
            val user = User.find { Users.login eq userLogin }.firstOrNull() ?:
            return@newSuspendedTransaction null
            val container = containerRepo.createContainer(containerDTO)
            println("${user.id.value}, ${container.id.value}")
            UserToContainer.new {
                userId = user.id.value
                containerId = container.id.value
            }
        }
    }


    suspend fun getContainers(userLogin: String) =
        newSuspendedTransaction(context, database) {
            val user = User.find { Users.login eq userLogin }.firstOrNull() ?:
                return@newSuspendedTransaction null
            val list = UserToContainer.find { UsersToContainers.user eq user.id.value }
                .toList().map { it.containerId}
            list.mapNotNull {
                Container.find { Containers.id eq it }.firstOrNull()?.asResponse()
            }
        }

//    suspend fun deleteContainer(id)
}