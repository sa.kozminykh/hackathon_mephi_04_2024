package com.example.data.container

import com.example.data.users.Users
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class Container(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Container>(Containers)

    var login by Containers.login
    var publicKey by Containers.publicKey
    var privateKey by Containers.privateKey

}