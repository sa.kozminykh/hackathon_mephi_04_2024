package com.example.data.container

import org.jetbrains.exposed.dao.id.IntIdTable

object Containers : IntIdTable() {
    val login = text("login")
    val publicKey = text("publicKey")
    val privateKey = text("privateKey")
}