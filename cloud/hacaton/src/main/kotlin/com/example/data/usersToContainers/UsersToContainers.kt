package com.example.data.usersToContainers

import com.example.data.container.Containers
import com.example.data.users.Users
import org.jetbrains.exposed.dao.id.IntIdTable

object UsersToContainers: IntIdTable() {
    val user = integer("user_id").references(Users.id)
    val container = integer("container_id").references(Containers.id)
}