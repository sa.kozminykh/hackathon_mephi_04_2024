package com.example.data.users

import org.jetbrains.exposed.dao.id.IntIdTable

object Users : IntIdTable() {
    val login = varchar("login", 50)
    val passwordHash = varchar("password_hash", 50)
}




