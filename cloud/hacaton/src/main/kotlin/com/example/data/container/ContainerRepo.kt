package com.example.data.container

import com.example.dtos.ContainerDTO
import com.example.dtos.asResponse
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import kotlin.coroutines.CoroutineContext

class ContainerRepo(
    private val database: Database,
    private val context: CoroutineContext = Dispatchers.IO
) {
    fun createContainer(container: ContainerDTO) = run {
        Container.new {
            login = container.login
            publicKey = container.publicKey
            privateKey = container.privateKey
        }
    }

    suspend fun getContainers(id: Int) = newSuspendedTransaction(context, database) {
        Container.find { Containers.id eq id }.toList().map { it.asResponse() }
    }

    suspend fun deleteContainer(id: Int) = newSuspendedTransaction(context, database) {
        Containers.deleteWhere { Containers.id eq id }
    }

}