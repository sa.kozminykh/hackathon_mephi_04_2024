package com.example.data.usersToContainers

import com.example.data.users.Users
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class UserToContainer(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<UserToContainer>(UsersToContainers)

    var userId by UsersToContainers.user
    var containerId by UsersToContainers.container
}