package com.example.data.users

import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import kotlin.coroutines.CoroutineContext

class UserRepo(
    private val database: Database,
    private val context: CoroutineContext = Dispatchers.IO
) {

    fun hashPassword(password: String): String {
        return password //TODO
    }
    suspend fun checkUserPasswordHash(login: String, passwordHash: String) =
        newSuspendedTransaction(context, database) {
            val passwordDBHash =
                User.find { Users.login eq login }.firstOrNull()?.passwordHash
//                Users.selectAll().where { Users.login eq login }
//                    .single()[Users.passwordHash]
            passwordDBHash == passwordHash
        }

    suspend fun checkUserPassword(login: String, password: String) =
        checkUserPasswordHash(login, hashPassword(password))


    suspend fun createUser(userLogin: String, password: String) =
        newSuspendedTransaction(context, database) {
            val exist = Users.selectAll().where { Users.login eq userLogin }
                .firstOrNull()?.get(Users.id) != null
            if (!exist) {
                User.new {
                    login = userLogin
                    passwordHash = hashPassword(password)
                }
            } else null
        }


}