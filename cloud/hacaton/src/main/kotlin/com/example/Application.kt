package com.example

import com.example.data.container.ContainerRepo
import com.example.data.users.UserRepo
import com.example.data.usersToContainers.UserToContainerRepo
import com.example.db.DatabaseSingleton
import com.example.plugins.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import kotlin.coroutines.coroutineContext

val userRepo = UserRepo(DatabaseSingleton.database)
val containerRepo = ContainerRepo(DatabaseSingleton.database)
val userToContainerRepo = UserToContainerRepo(
    userRepo, containerRepo, DatabaseSingleton.database
)
fun main() {
    embeddedServer(Netty, port = 8080, host = "0.0.0.0", module = Application::module)
        .start(wait = true)
}

fun Application.module() {
    configureSerialization()
    configureSecurity(userRepo)
    configureRouting(userRepo, userToContainerRepo)
}
