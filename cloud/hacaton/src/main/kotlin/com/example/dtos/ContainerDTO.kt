package com.example.dtos

import com.example.data.container.Container
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ContainerDTO(
    @SerialName("private_key_bytes_decoded")
    val login: String,
    @SerialName("public_key_bytes_decoded")
    val publicKey: String,
    @SerialName("login_server")
    val privateKey: String
)

fun Container.asResponse() = ContainerDTO(
    login, publicKey, privateKey
)