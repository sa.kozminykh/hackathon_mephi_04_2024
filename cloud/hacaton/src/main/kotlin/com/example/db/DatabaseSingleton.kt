package com.example.db

import com.example.data.container.Containers
import com.example.data.usersToContainers.UsersToContainers
import com.example.data.users.Users
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

object DatabaseSingleton {
    val database: Database
    init {
        val driverClassName = "org.h2.Driver"
        val jdbcURL = "jdbc:h2:~/h2-data"
//        val jdbcURL = "tcp://127.0.1.1:9092"
        database = Database.connect(jdbcURL, driverClassName,
            user = "hacaton", password = "password")
        transaction(database) {
            SchemaUtils.create(Users, Containers, UsersToContainers)
        }
    }
}