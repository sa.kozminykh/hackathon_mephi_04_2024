from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

def generate_key_pair(key_size):
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=key_size,
        backend=default_backend()
    )

    public_key = private_key.public_key()
    return private_key, public_key

def serialize_keys(password, private_key, public_key):
    private_key_bytes = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.BestAvailableEncryption(password.encode('utf-8'))
    )

    public_key_bytes = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )
    return private_key_bytes, public_key_bytes


def main():
    private_key, public_key = generate_key_pair(2048)
    password_private = input('Enter password for private key: ')
    private_key_bytes, public_key_bytes = serialize_keys(password_private, private_key, public_key)

    with open("private_key_bytes_device.pem", "wb") as f:
        f.write(private_key_bytes)

    with open("public_key_bytes_device.pem", "wb") as f:
        f.write(public_key_bytes)

main()