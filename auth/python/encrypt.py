from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

def encrypt(message, public_key_bytes):
    public_key = serialization.load_pem_public_key(public_key_bytes)

    if not isinstance(message, bytes): message = message.encode('utf-8')

    ciphertext = public_key.encrypt(
        message,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )
    return ciphertext

def main():
    with open("public_key_bytes_device.pem", "rb") as f1:
        public_key_bytes_device = f1.read()

    with open("message", "rb") as f2:
        ciphertext = encrypt(f2.read(), public_key_bytes_device)

    with open("ciphertext.pem", "wb") as f3:
        f3.write(ciphertext)

main()