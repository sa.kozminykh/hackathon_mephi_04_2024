import cryptography.exceptions
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
import secrets
import json
import requests

#optional
from datetime import datetime


open_db = {}

login_cloud = 'bob_cloud'
password_cloud = 'bob_on_cloud'

cloud_auth = {
    "login": login_cloud,
    "password": password_cloud
}

def generate_key_pair(key_size):
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=key_size,
        backend=default_backend()
    )

    public_key = private_key.public_key()
    return private_key, public_key

def serialize_keys(password, private_key, public_key):
    private_key_bytes = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.TraditionalOpenSSL,
        encryption_algorithm=serialization.BestAvailableEncryption(password.encode('utf-8'))
    )

    public_key_bytes = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )
    return private_key_bytes, public_key_bytes

def sign(message, private_key_bytes, password):
    try:
        private_key = serialization.load_pem_private_key(
            private_key_bytes,
            password=password.encode('utf-8')
        )

        if not isinstance(message, bytes): message = message.encode('utf-8')

        signature = private_key.sign(
            message,
            padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH
            ),
            hashes.SHA256()
        )
        return signature
    except ValueError:
        print("Wrong password for private key!")
        return bytes(0)

def verify_sign(message, signature, public_key_bytes):
    try:
        public_key = serialization.load_pem_public_key(public_key_bytes)

        if not isinstance(message, bytes): message = message.encode('utf-8')

        public_key.verify(
            signature,
            message,
            padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH
            ),
            hashes.SHA256()
        )
    except cryptography.exceptions.InvalidSignature:
        return 1
    return 0



def register_on_server(login, signature, meta, public_key_bytes):
    if login.lower() != login:
        print("Wrong data")
        return
    if verify_sign(login, signature, public_key_bytes):
        print("Registration failed: wrong signature")
        return

    if meta['smth'] != 'our_server':
        print("Wrong meta")
        return
    open_db[login] = [public_key_bytes, meta]
    print("Registration is successfully")
    return


def authenticate_on_server(login, private_key_bytes):
    if login not in open_db.keys():
        print("Wrong login")
        return 1
    else:
        print("The login has been found! Sending you an assertion to sign...")

    assertion = secrets.token_bytes(32) + open_db[login][0]

    #signing process
    password = str(input('Enter password to private key: '))
    signature = sign(assertion, private_key_bytes, password)

    #verification of correctness
    if verify_sign(assertion, signature, open_db[login][0]):
        print("Authentication failed: wrong signature")
        return 1

    print("Authentication is successfully")
    return 0


def main():
    private_key, public_key = generate_key_pair(2048)
    private_key_bytes, public_key_bytes = serialize_keys('finger1', private_key, public_key)

    # Hint: you can try these keys for authentication
    fake_private_key, fake_public_key = generate_key_pair(2048)
    fake_private_key_bytes, fake_public_key_bytes = serialize_keys('finger1', fake_private_key, fake_public_key)

    meta = {}
    meta['smth'] = 'our_server'
    meta['date'] = datetime.now()
    print(meta)
    login = 'bob'

    signature = sign(login, private_key_bytes, 'finger1')
    register_on_server(login, signature, meta, public_key_bytes)

    # Hint: you can change private key to check authenticate
    if not authenticate_on_server(login, private_key_bytes):
        #print(open_db)

        # need to convert bytes strings to str
        key_data = {
            "private_key_bytes_decoded": private_key_bytes.decode(),
            "public_key_bytes_decoded": public_key_bytes.decode(),
            "login_server": login
        }



        # register user on cloud
        print(requests.post(url='http://192.168.0.107:8080/user', json=cloud_auth))

        # sending key_data on cloud
        session = requests.Session()
        session.auth = (cloud_auth['login'], cloud_auth['password'])

        print(session.post('http://192.168.0.107:8080/container', json=key_data))

    with open("public_client.pem", "wb") as f:
        f.write(public_key_bytes)



main()

def get_from_cloud(login, password):
    session = requests.Session()
    session.auth = (login, password)

    key_data = session.get('http://192.168.0.107:8080/containers').json()

    #example
    print(key_data[0]['public_key_bytes_decoded'])
    print(key_data[0]['private_key_bytes_decoded'])

#get_from_cloud(cloud_auth['login'], cloud_auth['password'])