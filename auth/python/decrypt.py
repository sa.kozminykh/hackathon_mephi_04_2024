from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

def decrypt(password, private_key_bytes, message):
    private_key = serialization.load_pem_private_key(
        private_key_bytes,
        password=password.encode('utf-8')
    )

    if not isinstance(message, bytes): message = message.encode('utf-8')

    plaintext = private_key.decrypt(
        message,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )

    return plaintext

def main():
    with open("private_key_bytes_device.pem", "rb") as f1:
        private_key_bytes_device = f1.read()

    with open("ciphertext.pem", "rb") as f2:
        ciphertext = f2.read()

    password = input('Enter password for private key: ')
    print(decrypt(password, private_key_bytes_device, ciphertext))


main()